const express = require('express');
const logger = require('../../config/logger');

const router = express.Router();

const UserHttpController = require('../Controllers/Http/UserHttpController');



router.post('/', async (req, res, next) => {
    try {
        return res.status(200).send({ status: 'OK', message: 'servicio ejecutado con éxito' });
    } catch (error) {
        next(error)
    }
})

router.post('/error', async (req, res) => {
    try {
        return res.status(200).send(await new UserHttpController().getUsersError());
    } catch (error) {
        logger.error(error.message)
        return res.status(500).send({ status: 'ERROR', message: 'Servicio ejecutado con errores', detail: error.message })
    }
})

router.post('/error-handler', async (req, res, next) => {
    try {
        return res.status(200).send(await new UserHttpController().getUsersError());
    } catch (error) {
        next(error)
    }
})

router.post('/error-inesperado', async (req, res) => {
    return res.status(200).send(await new UserHttpController().getUsersError());
})

router.get('/compression', async (req, res, next) => {
    try {
        const result = await new UserHttpController().getAll();
        return res.status(200).send({ status: 'OK', message: 'servicio ejecutado con éxito', result });
    } catch (error) {
        next(error)
    }
})

module.exports = router;