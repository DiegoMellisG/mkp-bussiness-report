const { sleep } = require('../../Helpers/Utils');
const logger = require('../../../config/logger');

module.exports = class UserBusinessController {

    static async process(){
        logger.info(`PROCESANDO USARIO`);
        await sleep(20000);
        logger.info('USUARIO PROCESADO');
    }

}