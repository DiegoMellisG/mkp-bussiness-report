const UserBusinessController = require('../Business/UserBusinessController');
const ParameterException = require('../../Exceptions/ParameterException');

module.exports = class UserHttpController {

    constructor() { }

    async getAll() {
        const users = 'usuario.test;';
        return users.repeat(1000000)
    }

    async getUsersOk() {
        return {
            status: 'OK',
            message: 'Servicio ejecutado con éxito.'
        }
    }

    async test(body){
        // valido datos (entrada body, cantidad parametros)

        // ejecuto capa de negocio
        // const result = await capaNegocio.procesar();

        //respuesta
        return {
            status: 'OK',
            message: 'Servicio ejecutado con éxito.'
        }
    }

    async getUsersError() {
        throw new ParameterException('No ha ingresado los parametros necesarios', 401, 'Parametro para obtener usuarios inválidos')
    }

}