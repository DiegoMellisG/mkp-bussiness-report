module.exports = class ParameterException extends Error {

    constructor(message, status, summary){
        super();
        this.summary = summary;
        this.message = message;
        this.status = status;
        this.code = 'E_PARAMETER'
    }

}